class AddPreviewImageToProduct < ActiveRecord::Migration
  def up
    add_attachment :products, :preview
  end

  def down
    remove_attachment :products, :preview
  end
end
