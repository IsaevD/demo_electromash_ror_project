class ChangeCategoryType < ActiveRecord::Migration
  def change
    add_column :categories, :catalog_type_id, :integer
    remove_column :products, :catalog_type_id
  end
end
