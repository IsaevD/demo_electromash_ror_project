class CreateLocalizations < ActiveRecord::Migration
  def change
    create_table :localizations do |t|
      t.string :alias
      t.string :name
      t.text   :value

      t.timestamps
    end
  end
end
