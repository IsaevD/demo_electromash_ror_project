class CreateCatalogTypes < ActiveRecord::Migration
  def change
    create_table :catalog_types do |t|
      t.string :alias
      t.string :name

      t.timestamps
    end
  end
end
