class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :alias
      t.string :name
      t.text :description
      t.integer :product_type_id

      t.timestamps
    end
  end
end
