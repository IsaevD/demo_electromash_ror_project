class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.text :description
      t.date :date

      t.timestamps
    end
  end
end
