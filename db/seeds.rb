ModulesD::Engine.load_seed
SystemMainItemsD::Engine.load_seed
AccessRightsD::Engine.load_seed
SystemUserActionsLogD::Engine.load_seed

# Импорт информации о модулях
modules = {
    "articles" => {
        :module_name => "admin",
        :name => "Статьи",
        :description => "Предназначен для управления статьями",
        :git_url => ""
    },
    "requests" => {
        :module_name => "admin",
        :name => "Заявки",
        :description => "Предназначен для управления заявками",
        :git_url => ""
    }
}

modules.each do |key, value|
  if (ModulesD::ModuleItem.find_by(:alias => key).nil?)
    ModulesD::ModuleItem.new({ :alias => key, :module_name => value[:module_name], :name => value[:name], :description => value[:description], :git_url => value[:git_url] }).save
  end
end
puts "Custom modules import success"

# Импорт категорий товаров
catalog_types = {
    "electrolmash" => {
        :name  => "Товары Электромаша",
        :alias => "electromash"
    },
    "other" => {
        :name  => "Прочие товары",
        :alias => "other"
    }
}

catalog_types.each do | key , value |
  if (CatalogType.find_by(:alias => value[:alias]).nil?)
    CatalogType.new({ :alias => value[:alias] , :name => value[:name] }).save
  end
end
puts "Catalog types import success"

#Импорт типов товаров
products_types = {
    "3D" => {
        :name  => "3D",
        :alias => "3D"
    },
    "main" => {
        :name  => "Обычный",
        :alias => "usual"
    }
}

products_types.each do | key , value |
  if (ProductType.find_by(:alias => value[:alias]).nil?)
    ProductType.new({ :alias => value[:alias] , :name => value[:name] }).save
  end
end
puts "Product types import success"

localizations = {
    "about_us_slide" => {
        "first_description" => {
            :name => "Слайд \"О нас\", первое описание",
            :value => "Мы занимаемся разработкой, проектированием, унификацией и реализацией систем контроля и управления. Наша компания орентирована на импортозамещение электрооборудования."
        },
        "second_description" => {
            :name => "Слайд \"О нас\", второе описание",
            :value => "С момента основания, работаем на терри- тории столицы Удмуртской Республики и сотрудничаем с компаниями и предприятиями в различных сферах."
        }
    }
}
localizations.each do | key , category |
  category.each do | category_alias , values |
    if (Localization.find_by(:alias => key + "_" + category_alias)).nil?
      Localization.new({ :alias => key + "_" + category_alias , :name => values[:name] , :value => values[:value] }).save
    end
  end
end

puts "Localizations import success"