# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151203074527) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access_rights_d_access_rights", force: true do |t|
    t.integer  "system_user_role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "access_rights_d_module_rights", force: true do |t|
    t.integer  "access_right_id"
    t.integer  "module_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "articles", force: true do |t|
    t.string   "name"
    t.text     "content"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "catalog_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "alias"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.integer  "catalog_type_id"
    t.integer  "position"
  end

  create_table "localizations", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.text     "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "modules_d_module_items", force: true do |t|
    t.string   "alias"
    t.string   "module_name"
    t.string   "name"
    t.text     "description"
    t.string   "git_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news_items", force: true do |t|
    t.text     "description"
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.text     "description"
    t.integer  "product_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "category_id"
    t.string   "preview_file_name"
    t.string   "preview_content_type"
    t.integer  "preview_file_size"
    t.datetime "preview_updated_at"
    t.string   "doc_name"
    t.integer  "position"
  end

  create_table "requests", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_main_items_d_system_states", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_main_items_d_system_user_roles", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.text     "description"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_main_items_d_system_users", force: true do |t|
    t.string   "login"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_token"
    t.integer  "system_user_role_id"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "system_user_actions_log_d_actions_log_records", force: true do |t|
    t.integer  "system_user_id"
    t.integer  "log_record_state_id"
    t.datetime "action_date"
    t.integer  "module_item_id"
    t.integer  "entity_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_user_actions_log_d_log_record_states", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
