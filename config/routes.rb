Rails.application.routes.draw do

  root "index#index"
  match '/catalog/:alias', to: "catalog#item", via: "get"
  match '/other_products/:alias', to: "catalog#other_item", via: "get"
  match '/ajax_call_back_form', to: "index#send_call_back_form", via: "post"
  match '/download_price', to: "index#download_price", via: "get"

  scope "/admin" do
    mount ModulesD::Engine => "/"
    mount SystemMainItemsD::Engine => "/"
    mount SystemUsersAuthorizeD::Engine => "/"
    mount AccessRightsD::Engine => "/"
    mount SystemUserActionsLogD::Engine => "/"
    scope module: "admin" do
      resources :articles
      resources :requests
      resources :categories
      resources :product_types
      resources :products
      resources :news_items
      resources :localizations
    end
  end

end
