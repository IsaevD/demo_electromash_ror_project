class CatalogController < IndexController

  def item
    @alias = params[:alias]
    @product = Product.find_by(:alias => @alias)
    @title = @product.name
    @categories = @product.category.catalog_type.categories.order(:position)
  end


end