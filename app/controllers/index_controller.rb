class IndexController < ApplicationController

  def index
    @our_category = CatalogType.find_by(:alias => "electromash").categories.order(:position)
    @other_category = CatalogType.find_by(:alias => "other").categories.order(:position)
  end

  def send_call_back_form
    request = Request.new
    request.name = params[:name]
    request.phone = params[:phone]
    if request.save
      MainMailer.callback_mail(request).deliver
    end
    render :text => "Success"
  end

  def download_price
    send_file "#{Rails.root}/public/price.pdf", :type => "application/pdf", :x_sendfile => true
  end

end