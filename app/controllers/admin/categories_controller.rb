class Admin::CategoriesController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

  def init_variables

    @object = {
        :name => "categories",
        :entity => Category,
        :param_name => :category,
        :paths => {
            :all_path => Rails.application.routes.url_helpers.categories_path,
            :new_path => Rails.application.routes.url_helpers.new_category_path,
            :edit_path => Rails.application.routes.url_helpers.categories_path,
        },
        :fields => {
            :position => {
                :type => :string,
                :label => "Позиция",
                :show_in_card => true,
                :show_in_table => true
            },
            :alias => {
                :type => :string,
                :label => "Алиас",
                :show_in_card => true,
                :show_in_table => true
            },
            :name => {
                :type => :string,
                :label => "Название",
                :show_in_card => true,
                :show_in_table => true
            },
            :description => {
                :type => :text,
                :label => "Описание",
                :show_in_card => true,
                :show_in_table => false
            },
            :catalog_type_id => {
                :type => :collection,
                :label => "Тип каталога",
                :show_in_table => true,
                :show_in_card => true,
                :where_entity => CatalogType,
                :where_visible_field => :name,
                :where_statement => nil,
                :settings => {
                    :include_blank => false
                }
            }
        }
    }
    @fields = @object[:fields]
    @visible_fields = []
    @fields.each do |key, value|
      if value[:show_in_table]
        @visible_fields << key
      end
    end
    @entity = @object[:entity]
    @param_name = @object[:param_name]
    @path = @object[:paths][:all_path]
    @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
  end
end

