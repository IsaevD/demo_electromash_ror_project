class Admin::RequestsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

    def init_variables

      @object = {
          :name => "requests",
          :entity => Request,
          :param_name => :request,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.requests_path,
              :new_path => nil,
              :edit_path => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => "От кого",
                  :show_in_card => true,
                  :show_in_table => true
              },
              :phone => {
                  :type => :string,
                  :label => t('form.labels.phone'),
                  :show_in_card => true,
                  :show_in_table => true
              },
              :created_at => {
                  :type => :datetime,
                  :label => "Дата подачи",
                  :show_in_card => true,
                  :show_in_table => true
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end

