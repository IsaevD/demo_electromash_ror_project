class Admin::ProductTypesController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

  def init_variables

    @object = {
        :name => "product_types",
        :entity => ProductType,
        :param_name => :product_type,
        :paths => {
            :all_path => Rails.application.routes.url_helpers.product_types_path,
            :new_path => Rails.application.routes.url_helpers.new_product_type_path,
            :edit_path => Rails.application.routes.url_helpers.product_types_path,
        },
        :fields => {
            :alias => {
                :type => :string,
                :label => "Алиас",
                :show_in_card => true,
                :show_in_table => true
            },
            :name => {
                :type => :string,
                :label => "Название",
                :show_in_card => true,
                :show_in_table => true
            }
        }
    }
    @fields = @object[:fields]
    @visible_fields = []
    @fields.each do |key, value|
      if value[:show_in_table]
        @visible_fields << key
      end
    end
    @entity = @object[:entity]
    @param_name = @object[:param_name]
    @path = @object[:paths][:all_path]
    @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
  end
end

