class Admin::ProductsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create

    entity = @entity
    is_log = true
    path = @path
    is_redirect = true

    @item = entity.new(prepared_params(@param_name, @params_array))

    if !params[:product][:doc].nil?
      name = params[:product][:doc].original_filename
      directory = "public/system/upload"
      path1 = File.join(directory, name)
      File.open(path1, "wb") { |f| f.write(params[:product][:doc].read) }
      @item.doc_name = name
    end

    if @item.save
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :dual_list
            if settings[:show_in_card]
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], @item.id)
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
      end
      if (is_redirect)
        redirect_to path
      end
    else
      render "new"
    end

    #create_item(@entity, @path, true)
  end

  def update

    id = params[:id]
    entity = @entity
    path = @path
    is_log = true

    @item = entity.find(id)

    @fields.each do |field, settings|
      entity_params = params[@param_name]
      case settings[:type]
        when :image
          if entity_params[field] == "nil"
            params[@param_name][field] = nil
          end
        when :dual_list
          if settings[:show_in_card]
            current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
            current_values.destroy_all
            values = entity_params[field]
            if !values.nil?
              values.each do |key, value|
                settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
              end
            end
          end
        when :image_list
          if !entity_params[settings[:image_entity_field]].nil?
            entity_params[settings[:image_entity_field]].each do |key, value|
              if value == "nil"
                @fields[field][:image_entity].find(key).destroy
              end
            end
          end
          if !entity_params[field].nil?
            entity_params[field].each do |value|
              image_list = @fields[field][:image_entity].new
              image_list.update_attribute(@fields[field][:entity_field], params[:id])
              image_list.update_attribute(@fields[field][:image_entity_field], value)
              image_list.save
            end
          end
      end
    end

    if !params[:product][:doc].nil?
      name = params[:product][:doc].original_filename
      directory = "public/system/upload"
      path1 = File.join(directory, name)
      File.open(path1, "wb") { |f| f.write(params[:product][:doc].read) }
      @item.doc_name = name
    else
      if (params[:delete_specification] == "1")
        @item.doc_name = nil
      end
    end

    if @item.update_attributes(prepared_params(@param_name, @params_array))
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
      end
      redirect_to path
    else
      render "new"
    end

    #update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

  def init_variables

    @object = {
        :name => "products",
        :entity => Product,
        :param_name => :product,
        :paths => {
            :all_path => Rails.application.routes.url_helpers.products_path,
            :new_path => Rails.application.routes.url_helpers.new_product_path,
            :edit_path => Rails.application.routes.url_helpers.products_path,
        },
        :fields => {
            :position => {
                :type => :string,
                :label => "Позиция",
                :show_in_card => true,
                :show_in_table => true
            },
            :alias => {
                :type => :string,
                :label => "Алиас",
                :show_in_card => true,
                :show_in_table => true
            },
            :name => {
                :type => :string,
                :label => "Название",
                :show_in_card => true,
                :show_in_table => true
            },
            :description => {
                :type => :html,
                :label => "Описание",
                :show_in_card => true,
                :show_in_table => false
            },
            :image => {
                :type => :image,
                :label => "Изображение",
                :show_in_card => true,
                :show_in_table => false
            },
            :preview => {
                :type => :image,
                :label => "Миниатюра",
                :show_in_card => true,
                :show_in_table => false
            },
            :product_type_id =>  {
                :type => :collection,
                :label => "Тип продукта",
                :show_in_table => true,
                :show_in_card => true,
                :where_entity => ProductType,
                :where_visible_field => :name,
                :where_statement => nil,
                :settings => {
                    :include_blank => false
                }
            },
            :category_id =>  {
                :type => :collection,
                :label => "Категория продукта",
                :show_in_table => true,
                :show_in_card => true,
                :where_entity => Category,
                :where_visible_field => :name,
                :where_statement => nil,
                :settings => {
                    :include_blank => false
                }
            }
        }
    }
    @fields = @object[:fields]
    @visible_fields = []
    @fields.each do |key, value|
      if value[:show_in_table]
        @visible_fields << key
      end
    end
    @entity = @object[:entity]
    @param_name = @object[:param_name]
    @path = @object[:paths][:all_path]
    @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
  end
end

