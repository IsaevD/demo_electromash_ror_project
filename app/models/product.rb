class Product < ActiveRecord::Base
  has_attached_file :image, :styles => { :list => "300x140" , :preview => "128x55" , :thumb => "100x100" } #, :default_url => "/images/system_main_items/no_user_avatar.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  has_attached_file :preview, :styles => { :list => "300x140" , :preview => "128x55" , :thumb => "100x100" } #, :default_url => "/images/system_main_items/no_user_avatar.png"
  validates_attachment_content_type :preview, :content_type => /\Aimage\/.*\Z/

  belongs_to :category
  belongs_to :product_type

  validates :name, :length => { :maximum => 30 }
end
