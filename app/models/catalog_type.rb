class CatalogType < ActiveRecord::Base
  has_many :categories

  validates :name, :length => { :maximum => 25 }
end
