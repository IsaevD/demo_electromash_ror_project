class Category < ActiveRecord::Base
  has_many :products
  belongs_to :catalog_type

  validates :name, :length => { :maximum => 25 }
end
