class MainMailer < ActionMailer::Base
  default from: "site@kb-elektromash.ru"
  default to: "info@kb-elektromash.ru"

  def callback_mail(request)
    @request = request
    mail(subject: "Заказ обратного звонка Электромаш")
  end
end