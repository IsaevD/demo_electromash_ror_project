function initSlider() {

    object = $(".slider");
    children = object.children(".slider_children");
    $(children).children(".screen").first().addClass("current");
    reinitChildren();

    $(document).keydown(function(e) {
        switch(e.which) {
            case 37: // left
                previousSlide();
                break;

            case 39: // right
                nextSlide();
                break;

            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });

    $(".arrows .left_arrow, .labels .left_arrow").click(function(){
        previousSlide();
    });

    $(".arrows .right_arrow, .labels .right_arrow").click(function(){
        nextSlide();
    });

    function nextSlide() {
        $(".arrows .right_arrow").show();
        $(".arrows .left_arrow").show();
        slideAnimation("right");
    }


    function previousSlide() {
        $(".arrows .right_arrow").show();
        $(".arrows .left_arrow").show();
        slideAnimation("left");
    }

    function slideAnimation(type) {

        admixture = 0;

        current = $($(children).children(".screen.current")).index();
        length = $(children).children(".screen").length - 1;
        width = $(children).children(".screen.current").width();
        if (length > 2) {
            switch (type) {
                case "right":
                    if (current == length - admixture)
                        current = 0;
                    else
                        current = current + 1;
                    break;
                case "left":
                    if (current == 0)
                        current = length - admixture;
                    else
                        current = current - 1;
                    break;
            }
            if ((type == "left") && ($(".screen.current").prev().length != 0)) {
                $(children).animate({left: -( current * width )}, 800);
                $($(children).children(".screen")).removeClass("current");
                $($(children).children(".screen")[current]).addClass("current");
            }
            if ((type == "right") && ($(".screen.current").next().length != 0)) {
                $(children).animate({left: -( current * width )}, 800);
                $($(children).children(".screen")).removeClass("current");
                $($(children).children(".screen")[current]).addClass("current");
            }
        }

        if ($(".screen.current").next().length == 0) {
            $(".arrows .right_arrow").hide();
        }
        if ($(".screen.current").prev().length == 0) {
            $(".arrows .left_arrow").hide();
        }

    }

    function reinitChildren() {
        width = $(children).children(".screen.current").width();
        current = $($(children).children(".screen.current")).index();
        $(children).children(".screen").width($(window).width());
        $(children).css("left", -( current * width ));
    }

    $(window).resize(function(){
        reinitChildren();
    });
}