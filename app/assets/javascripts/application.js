// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require slider

$(function(){

    initializePopup();
    initializeCallbackForm();
    initSlider();


    $(".new_screen_pagination .paginate_item").click(function(){
        if (!$(this).hasClass("active")) {
            $(".screen_pagination .paginate_item").removeClass("active");
            $(this).addClass("active");
            $(".screen").hide();
            $($(".screen")[$(this).index()]).show();
        }
    });

    var url = window.location.href;
    var hash = url.substring(url.indexOf("#")+1);
    if (url.indexOf("#") < 0) {
        hash = "";
    } else {
        $("nav li a.animate[href='"+url+"']").addClass("active");
    }
    initializeActiveMenu(hash, false);
    $("nav li a.animate").click(function(){
        initializeActiveMenu($(this).attr("href").substring($(this).attr("href").indexOf("#")+1), true);
        $("nav li a.animate").removeClass("active");
        $(this).addClass("active");
        return false;
    });

    /*
    $(".slider_children").animate({left: -( current * $(".screen").width() )}, 800);
    $(".screen.current").removeClass("current").next().addClass("current");
    */

});

function test_ajax () {
    send_ajax( "/ajax_call_back_form", "test", function(data) {console.log(data);} );
}

function send_ajax( url, data, callback ) {
    $.ajax({
        url: url,
        type: "POST",
        cache: false,
        data: data,
        success: function(data){
            callback(data);
        }
    });
}


function initializePopup() {

    $(".popup").click(function(){
        $(this).hide();
        $("#order_popup .cnt").show();
        $("#order_popup .triumph").hide();
    });

    $(".popup .popup_cnt").click(function(e){
        e.stopPropagation();
    });

    /*
    $(".popup").click(function(){
        $(".popup").hide();
    });
    */

    $("#callback").click(function(){
        $("#order_popup").show();
        $("#name").val("");
        $("#phone").val("");
    });

}

function initializeActiveMenu(hash, animate) {
    $(".arrows .right_arrow").show();
    $(".arrows .left_arrow").show();
    if (hash != "") {
        if ($("#" + hash).length != 0) {
            if (animate)
                $(".slider_children").animate({left: -1 * (parseInt($("#" + hash).index())*$(".screen").width())}, 800);
            else
                $(".slider_children").css("left", -1 * (parseInt($("#" + hash).index())*$(".screen").width()));

            $(".screen.current").removeClass("current");
            $("#" + hash).addClass("current");
            $()
        }
    } else {
        if (animate)
            $(".slider_children").animate({left: -1 * $(".screen").width() }, 800);
        else
            $(".slider_children").css("left", -1 * $(".screen").width());

        $(".screen.current").removeClass("current");
        $("#main").addClass("current");
    }
    if ($(".screen.current").next().length == 0) {
        $(".arrows .right_arrow").hide();
    }
    if ($(".screen.current").prev().length == 0) {
        $(".arrows .left_arrow").hide();
    }

}



function initializeCallbackForm() {
    $("#order_popup form").submit(function() {
        $("#order_popup form .error").hide();
        if ($("#name").val() == "") {
            $("#name").next().show();
        } else {
            if ($("#phone").val() == "") {
                $("#phone").next().show();
            } else {
                postData = $(this).serializeArray();
                send_ajax( $(this).attr("action"), postData, function(data) {
                    $("#order_popup .cnt").hide();
                    $("#order_popup .triumph").show();
                });
            }
        }

        return false;
    });
}


//= require turbolinks



