module ApplicationHelper
  def get_site_title ( product_name )
    if product_name.nil?
      return "Конструкторское бюро Электромаш"
    else
      return "Конструкторское бюро Электромаш - #{product_name}"
    end
  end
end
